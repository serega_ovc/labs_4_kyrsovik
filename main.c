#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N 81
typedef struct MusicalComposition MusicalComposition;
// Описание структуры MusicalComposition
struct MusicalComposition{
    char name[N];
    char author[N];
    int year;

    MusicalComposition *next;
    MusicalComposition *past;
};

// Создание структуры MusicalComposition

MusicalComposition* createMusicalComposition(char* name, char* author,int year){
    MusicalComposition * music = malloc(sizeof(MusicalComposition));

    strncpy(music->name, name, N);
    strncpy(music->author, author, N);
    music->year = year;
    music->next = NULL;
    music->past = NULL;

    return music;
}

// Функции для работы со списком MusicalComposition

MusicalComposition* createMusicalCompositionList(char** array_names, char** array_authors, int* array_years, int n){
    MusicalComposition* array[n];

    array[0] = createMusicalComposition(array_names[0],array_authors[0],array_years[0]);

    for(int i = 1;i<n;i++){
        array[i] = createMusicalComposition(array_names[i],array_authors[i],array_years[i]);
        array[i-1]->next = array[i];
        array[i]->past = array[i-1];
    }

    return array[0];
}

void push(MusicalComposition* head, MusicalComposition* element){
    if(head  == NULL){
        head = element;
        return;
    }

    MusicalComposition* current_element = head;

    while(current_element->next != NULL){
        current_element = current_element->next;
    }

    current_element->next = element;
    element->past = current_element;
}

void removeEl(MusicalComposition* head, char* name_for_remove){
    MusicalComposition* current_element = head;
    MusicalComposition* local_copy;
    
    while(current_element != NULL){
        if(!strcmp(current_element->name,name_for_remove)){
            current_element->next->past = current_element->past;
            current_element->past->next = current_element->next;
            local_copy = current_element->next;

            free(current_element);
            current_element = local_copy;
        }else{
            current_element = current_element->next;
        }
    }
}

int count(MusicalComposition* head){
    MusicalComposition* current_element = head;
    int i = 0;

    while(current_element != NULL){
        i++;
        current_element = current_element->next;
    }
    return i;
}

void print_names(MusicalComposition* head){
    MusicalComposition* current_element = head;

    while(current_element != NULL){
        printf("%s\n", current_element->name);
        current_element = current_element->next;
    }
}

void printList(MusicalComposition* head){
    while(head){
        printf("[%s, %s, %i]\n",head->name,head->author,head->year);
        head=head->next;
    }
}

MusicalComposition* swap_head_and_last(MusicalComposition* head){
    MusicalComposition* last_element = head;
    while(last_element->next!=NULL){
        last_element=last_element->next;
    }
   
    last_element->next = head->next;
    head->past = last_element->past;
    
    head->next->past= last_element;
    last_element->past->next = head;

    head->next =NULL;
    last_element->past =NULL;

    return last_element;
}

void delete_elements(MusicalComposition* head){
    MusicalComposition* last_element = head;
    while(last_element->next!=NULL){
        last_element=last_element->next;
    }
    head->next = head->next->next;
    head->next->past = head;

    last_element->past = last_element->past->past;
    last_element->past->next = last_element;

}


int main(){
    int length;
    scanf("%d\n", &length);  

    char** names = (char**)malloc(sizeof(char*)*length);
    char** authors = (char**)malloc(sizeof(char*)*length);
    int* years = (int*)malloc(sizeof(int)*length);

    for (int i=0;i<length;i++)
    {
        char name[80];
        char author[80];

        fgets(name, 80, stdin);
        fgets(author, 80, stdin);
        fscanf(stdin, "%d\n", &years[i]);

        (*strstr(name,"\n"))=0;
        (*strstr(author,"\n"))=0;

        names[i] = (char*)malloc(sizeof(char*) * (strlen(name)+1));
        authors[i] = (char*)malloc(sizeof(char*) * (strlen(author)+1));

        strcpy(names[i], name);
        strcpy(authors[i], author);

    }
    MusicalComposition* head = createMusicalCompositionList(names, authors, years, length);

    /*char name_for_push[80];
    char author_for_push[80];
    int year_for_push;

    char name_for_remove[80];

    fgets(name_for_push, 80, stdin);
    fgets(author_for_push, 80, stdin);
    fscanf(stdin, "%d\n", &year_for_push);
    (*strstr(name_for_push,"\n"))=0;
    (*strstr(author_for_push,"\n"))=0;

    MusicalComposition* element_for_push = createMusicalComposition(name_for_push, author_for_push, year_for_push);

    fgets(name_for_remove, 80, stdin);
    (*strstr(name_for_remove,"\n"))=0;

    printf("%s %s %d\n", head->name, head->author, head->year);
    int k = count(head);

    printf("%d\n", k);
    push(head, element_for_push);
*/
   // int k = count(head);
   // printf("%d\n", k);

  //  removeEl(head, name_for_remove); 
  //  print_names(head);
    printf("---------\n");
    printList(head);
    printf("---------\n");

    head = swap_head_and_last(head);
    printf("---------\n");
    printList(head);
    printf("---------\n");
   // k = count(head);
   // printf("%d\n", k);
    delete_elements(head);
    printf("---------\n");
    printList(head);
    printf("---------\n");

    for (int i=0;i<length;i++){
        free(names[i]);
        free(authors[i]);
    }
    free(names);
    free(authors);
    free(years);

    return 0;

}